import axios from 'axios';
import UserService from '../services/user.service';

const API_URL = 'https://blog.lockmark.fr/api/';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'login_check', {
        username: user.username,
        password: user.password
      }, { withCredentials: true })
      .then(() => { 
        var user = UserService.getMe()
          .then(responseuser => { 
            return  responseuser; 
          });
          
          return user; 
      });

  }

  logout() {
    return axios.get(API_URL + "users/logout", { withCredentials: true });
  }

  register(user) {
    return axios.post(API_URL + 'users', {
      username: user.username,
      email: user.email,
      password: user.password,
      name: user.name
    });
  }
}

export default new AuthService();
