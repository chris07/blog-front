import axios from 'axios';

const API_URL = 'https://blog.lockmark.fr/api/';

class ChartsService{

  getTotalArticles(){
    return axios.get(API_URL + "articles");
  }

  getTotalComments(){
    return axios.get(API_URL + "comments");
  }
  
}

export default new ChartsService();
