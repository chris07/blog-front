import axios from 'axios';

const API_URL = 'https://blog.lockmark.fr/api/';

class UserService {

  //Récupère tous les articles
  getPublicContent() {
    return axios.get(API_URL + "articles/");
  }

  //retourne l'utilisateur connecté
  getMe(){
    return  axios
      .get(API_URL + "users/" + "me", { withCredentials: true })
      .then(response => { 
        return response.data;
      },
      error => {
          (error.response && error.response.data) ||
          error.message ||
          error.toString();
          return null;
      }
    );
  }

  //Retourne les infos de l'utilisateur pour le dashboard
  getSuperAdminBoard(id) {
    return axios.get(API_URL + "users/" + id, { withCredentials: true });
  }

  //Retourne les infos de l'utilisateur pour le dashboard
  getAdminBoard(id) {
    return axios.get(API_URL + "users/" + id, { withCredentials: true });
  }

  //Retourne les articles de l'utilisateur
  getMyArticles(id) {
    return axios.get(API_URL + "users/" + id + "/articles", { withCredentials: true });
  }

  //Retourne tous les utilisateurs
  getAll(){
     return axios.get(API_URL + "users", { withCredentials: true });
  }

  //Retourne un utilisateur
  getOne(id) {
      return axios.get(API_URL + "users/" + id, { withCredentials: true });
  }

  //Modifie les roles d'un utilisateur
  update(id, user) {
   return axios.patch(API_URL + "users/" + id , {
        roles: user
    }, {headers:{'Content-Type': 'application/merge-patch+json' }, withCredentials: true});
  }

  //Modifie le profile d'un utilisateur
  updateProfile(id, user) {
   return axios.patch(API_URL + "users/" + id , {
       email: user.email,
       username: user.username,
       name: user.name
    }, {headers:{'Content-Type': 'application/merge-patch+json' }, withCredentials: true});
  }

  //Modifie le mot de passe et l'utilisateur
  updatePassword(id, user) {
   return axios.put(API_URL + "users/" + id , {
       email: user.email,
       password: user.password,
       roles: user.roles,
       username: user.username,
       name: user.name
    }, {withCredentials: true});
  }

  //Supprime un utilisateur
  delete(id) {
      return axios.delete(API_URL + "users/" + id, { withCredentials: true });
  }
}

export default new UserService();
