import axios from 'axios';

const API_URL = 'https://blog.lockmark.fr/api/';

class ImageService{

  getAll(user){
  	return axios.get(API_URL + "users/" + user + "/images.json");
  }

  getTotal(){
    return axios.get(API_URL + "media_objects.json");
  }

  getOne(id) {
    return axios.get(API_URL + "media_objects/" + id, { withCredentials: true });
  }

  create(formData) {
    return axios.post(API_URL + "media_objects", formData, { headers: {'Content-Type': 'multipart/form-data'}, withCredentials: true });
  }

  delete(id) {
    return axios.delete(API_URL + "media_objects/" + id, { withCredentials: true });
  }
}

export default new ImageService();
