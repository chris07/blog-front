import axios from 'axios';

const API_URL = 'https://blog.lockmark.fr/api/';

class CommentService {

  getAll(){
  	return axios.get(API_URL + "comments");
  }

  getOne(id) {
    return axios.get(API_URL + "comments/" + id);
  }

  create(comment, article, parent) {
    return axios.post(API_URL + "comments", {
      content: comment,
      article: "/api/articles/" + article,
      parent: parent
    }, { withCredentials: true });
  }

  delete(id) {
   return axios.delete(API_URL + "comments/" + id, { withCredentials: true });
  }
}

export default new CommentService();
