import axios from 'axios';

const API_URL = 'https://blog.lockmark.fr/api/';

class TagService {

  getAll(){
  	return axios.get(API_URL + "tags.json");
  }

  getOne(id) {
    return axios.get(API_URL + "tags/" + id );
  }

  create(tag) {
    return axios.post(API_URL + "tags", {
      label: tag.label
    }, { withCredentials: true });
  }

  createByArticle(tag) {
    return axios.post(API_URL + "tags", {
      label: tag
    }, { withCredentials: true });
  }

  update(id, tag) {
   return axios.put(API_URL + "tags/" + id, {
      label: tag.label 
    }, { withCredentials: true });
  }

  delete(id) {
   return axios.delete(API_URL + "tags/" + id, { withCredentials: true });
  }
}

export default new TagService();
