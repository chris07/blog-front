import axios from 'axios';

const API_URL = 'https://blog.lockmark.fr/api/';

class CategorieService {

  getAll(){
  	 return axios.get(API_URL + "categories.json");
  }

  getOne(id) {
      return axios.get(API_URL + "categories/" + id);
  }

  create(categorie) {
    return axios.post(API_URL + "categories", {
        name: categorie.name
    }, { withCredentials: true });
  }

  update(id, categorie) {
   return axios.put(API_URL + "categories/" + id, {
        name: categorie.name
    }, { withCredentials: true });
  }

  delete(id) {
      return axios.delete(API_URL + "categories/" + id, { withCredentials: true });
  }
}

export default new CategorieService();
