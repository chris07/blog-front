import axios from 'axios';

const API_URL = 'https://blog.lockmark.fr/api/';

class PostService {

  getAll(){
  	return axios.get(API_URL + "articles");
  }

  getAllSubmited(){
    return axios.get(API_URL + "articles?isSubmited=true");
  }

  getOne(id) {
    return axios.get(API_URL + "articles/" + id + ".json");
  }

  searchtitle(param){
     return axios.get(API_URL + "articles?title=" + param + "&isPublished=true&pagination=false");
  }
  searchtags(param){
    return axios.get(API_URL + "articles?tags.label=" + param + "&isPublished=true&pagination=false");
  }

  create(article) {
    return axios.post(API_URL + "articles", {
      title: article.title,
      content: article.content,
      picture: article.picture,
      categorie: "/api/categories/" + article.categorie,
      tags: article.tags
    }, { withCredentials: true });
  }

  updateStatus(id, status) {
   return axios.patch(API_URL + "articles/" + id , {
        isPublished: status,
        isSubmited: false
    }, {headers:{'Content-Type': 'application/merge-patch+json' }, withCredentials: true});
  }

  submit(id, status) {
   return axios.patch(API_URL + "articles/" + id , {
        isSubmited: status
    }, {headers:{'Content-Type': 'application/merge-patch+json' }, withCredentials: true});
  }

  update(id, article,content, tags, pic) {
   return axios.put(API_URL + "articles/" + id + ".json", {
      title: article.title,
      content: content,
      picture: pic,
      categorie: "/api/categories/" + article.categorie,
      tags: tags
    }, { withCredentials: true });
  }

  delete(id) {
   return axios.delete(API_URL + "articles/" + id, { withCredentials: true });
  }
}

export default new PostService();
