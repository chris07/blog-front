import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
      path: '/',
      name: 'home',
      component:() => import('../views/Home.vue')
    },
    { path: '/404', name: '404', component: () => import('../components/404.vue') },  
    { path: '*', redirect: '/404' },
    {
      path: '/home',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/search',
      component: () => import('../views/Search.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/users/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/users/Register.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/users/Profile.vue'),
      beforeEnter: (to,from,next) => {    
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')   
        if(roles.includes('ROLE_USER')){
          next()
        }
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('../views/boards/BoardAdmin.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/editor',
      name: 'editor',
      component: () => import('../views/boards/BoardEditor.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_EDITOR')){
          next()
        }
      }
    },
    {
      path: '/superadmin',
      name: 'superadmin',
      component: () => import('../views/boards/BoardSuperAdmin.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/images',
      name: 'admin/images',
      component: () => import('../views/images/Images.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN') || roles.includes('ROLE_EDITOR')){
          next()
        }
      }
    },
    {
      path: '/admin/image/:id',
      name: 'show/images',
      component: () => import('../views/images/Image.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN') || roles.includes('ROLE_EDITOR')){
          next()
        }
        else{
          next({ name: '404' })
        }
      }
    },
    {
      path: '/admin/categories',
      name: 'admin/categories',
      component: () => import('../views/categories/Categories.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/categorie/create',
      name: 'categorie/create',
      component: () => import('../views/categories/CategorieCreate.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/categorie/:id',
      name: 'categorie/articles',
      component: () => import('../views/categories/Categorie.vue'),
      props: true
    },
    {
      path: '/admin/categorie/:id',
      name: 'categorie/update',
      component: () => import('../views/categories/CategorieUpdate.vue'),
      props: true,
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/tags',
      name: 'admin/tags',
      component: () => import('../views/tags/Tags.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN') || roles.includes('ROLE_EDITOR')){
          next()
        }
      }
    },
    {
      path: '/admin/tag/create',
      name: 'tag/create',
      component: () => import('../views/tags/TagCreate.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN') || roles.includes('ROLE_EDITOR')){
          next()
        }
      }
    },
    {
      path: '/admin/tag/:id',
      name: 'tag/update',
      component: () => import('../views/tags/TagUpdate.vue'),
      props: true,
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN') || roles.includes('ROLE_EDITOR')){
          next()
        }
      }
    },
    {
      path: '/superadmin/users',
      name: 'superadmin/users',
      component: () => import('../views/users/Users.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/users',
      name: 'admin/users',
      component: () => import('../views/users/Users.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/superadmin/user/:id',
      name: 'superuser/update',
      component: () => import('../views/users/UserUpdate.vue'),
      props: true,
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/user/:id',
      name: 'user/update',
      component: () => import('../views/users/UserUpdate.vue'),
      props: true,
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/profil',
      name: 'user/profil',
      component: () => import('../views/users/profileUpdate.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_USER')){
          next()
        }
      }
    },
    {
      path: '/admin/article/create',
      name: 'article/create',
      component: () => import('../views/articles/ArticleCreate.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN') || roles.includes('ROLE_EDITOR')){
          next()
        }
      }
    },
    {
      path: '/article/:id',
      name: 'article/show',
      component: () => import('../views/articles/Article.vue')
    },
    {
      path: '/admin/article/:id',
      name: 'article/update',
      component: () => import('../views/articles/ArticleUpdate.vue'),
      props: true,
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN') || roles.includes('ROLE_EDITOR')){
          next()
        }   
      }
    },
    {
      path: '/admin/articles',
      name: 'admin/articles',
      component: () => import('../views/articles/Articles.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/articles/publiables',
      name: 'admin/articles/publiables',
      component: () => import('../views/articles/ArticlesPubliables.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/comments',
      name: 'admin/comments',
      component: () => import('../views/comments/Comments.vue'),
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/admin/comment/:id',
      name: 'comment/show',
      component: () => import('../views/comments/Comment.vue'),
      props: true,
      beforeEnter: (to,from,next) => {
        let rls = window.localStorage.getItem('role')
        let roles = rls.split(',')
        if(roles.includes('ROLE_SUPER_ADMIN') || roles.includes('ROLE_ADMIN')){
          next()
        }
      }
    },
    {
      path: '/cgi',
      name: 'cgi',
      component: () => import('../views/cgi.vue'),
      props: true,
    },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

  
router.afterEach(() => {
  let  contm1, contm2;

  if(document.getElementById("bouton-menu1") !== undefined){
    contm1 = document.getElementById("navbarCollapse");
  }

  if(document.getElementById("menu") !== undefined){
    contm2 = document.getElementById("navbarCollapse");
  }

  if(contm1.classList.contains('show') === true){
    contm1.classList.remove('show');
  }

  if(contm2.classList.contains('full') === true){
    contm1.classList.remove('full');
  }

  window.scrollTo(0, 0);

})
  

export default router
