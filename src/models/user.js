export default class User {
  constructor(username, email, password, roles, name, id) {
    this.username = username;
    this.email = email;
    this.password = password;
    this.roles = roles;
    this.name = name;
    this.id = id;
  }
}
