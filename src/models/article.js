export default class Article {
  constructor(titre, content, categorie, id, pic) {
    this.titre = titre;
    this.content = content;
    this.isPublished = false;
    this.categorie = categorie;
    this.tags = [];
    this.id = id;
    this.picture = pic;
  }
}
