import PostService from '../services/post.service';

const initialState = { search: {resultsTitle:null,resultsTags:null}};

export const search = {
  namespaced: true,
  state: initialState,
  actions: {
    setTitle({ commit }, param){
      return PostService.searchtitle(param).then(
        Title => {
          commit('setTitle', Title);
          return Promise.resolve(Title);
        },
        error => {
          commit('setError');
          return Promise.reject(error);
        }
      );
    },
    setTags({ commit }, param){
      return PostService.searchtags(param).then(
        Tags => {
          commit('setTags', Tags);
          return Promise.resolve(Tags);
        },
        error => {
          commit('setError');
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {
    setTitle(state, Title) {
      state.search.resultsTitle = Title.data['hydra:member'];
    },
    setTags(state, Tags) {
      state.search.resultsTags = Tags.data['hydra:member'];
    },
    setError(state) {
      state.resultsTitle = null
      state.resultsTags = null
    }
  }
};
