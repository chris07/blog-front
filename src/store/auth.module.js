import AuthService from '../services/auth.service';
import UserService from '../services/user.service';

const initialState = { status: { loggedIn: false }, user: null };



export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    set({ commit }){
      return UserService.getMe().then(
        user => {
          if(user !== null){
            commit('set', user);
            return Promise.resolve(user);
          }
        },
        error => {
          commit('setError');
          return Promise.reject(error);
        }
      );
    },
    login({ commit }, user) {
      return AuthService.login(user).then(
        user => {
          commit('loginSuccess', user);
          return Promise.resolve(user);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }) {
      AuthService.logout();
      commit('logout');
    },
    register({ commit }, user) {
      return AuthService.register(user).then(
        response => {
          commit('registerSuccess');
          return Promise.resolve(response.data);
        },
        error => {
          commit('registerFailure');
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {
    set(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    setError(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    registerSuccess(state) {
      state.status.loggedIn = false;
    },
    registerFailure(state) {
      state.status.loggedIn = false;
    }
  }
};
