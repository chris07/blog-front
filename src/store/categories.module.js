import CategorieService from '../services/categorie.service';

const initialState = { categories: null };

export const categories = {
  namespaced: true,
  state: initialState,
  actions: {
    set({ commit }){
      return CategorieService.getAll().then(
        categories => {
          commit('set', categories);
          return Promise.resolve(categories);
        },
        error => {
          commit('setError');
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {
    set(state, categories) {
      state.categories = categories.data;
    },
    setError(state) {
      state.categories = null
    }
  }
};
