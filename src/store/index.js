import Vue from 'vue';
import Vuex from 'vuex';

import { auth } from './auth.module';
import { categories } from './categories.module';
import { tags } from './tags.module';
import { search } from './search.module';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    categories,
    tags,
    search
  }
});
