import TagService from '../services/tag.service';

const initialState = { tags: null };

export const tags = {
  namespaced: true,
  state: initialState,
  actions: {
    set({ commit }){
      return TagService.getAll().then(
        tags => {
          commit('set', tags);
          return Promise.resolve(tags);
        },
        error => {
          commit('setError');
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {
    set(state, tags) {
      state.tags = tags.data;
    },
    setError(state) {
      state.tags = null
    }
  }
};