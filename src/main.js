import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import VeeValidate from 'vee-validate';
import Vuelidate from 'vuelidate';
import { library } from '@fortawesome/fontawesome-svg-core';
import CKEditor from 'ckeditor4-vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faHome,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt,
  faAngleDown,
  faAngleUp,
  faCopy,
  faUserSecret,
  faUserNinja,
  faFeatherAlt,
  faPlus,
  faCookieBite,

} from '@fortawesome/free-solid-svg-icons';
import Select2 from 'v-select2-component';

library.add(faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt, faAngleDown, faAngleUp, faCopy, faUserSecret, faUserNinja, faFeatherAlt, faPlus, faCookieBite);

Vue.config.productionTip = false;

Vue.use(VeeValidate);
Vue.use( CKEditor );
Vue.use(Vuelidate)

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('Select2', Select2);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
